# Easybank landing page 

## Table of contents

  - [The challenge](#the-challenge)
  - [Links](#links)
  - [Built with](#built-with)
  - [Author](#author)



### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page

### Links

- Solution URL: [Add solution URL here](https://gitlab.com/IkwemogenaA/my_project)
- Live Site URL: [Add live site URL here](https://live-site.com)

## My process

### Built with

- Semantic HTML5 markup
- Tailwind CSS
- Flexbox
- CSS Grid
- JavaScript

### Continued development


## Author

- Website - [Ikwemogena](https://www.your-site.com)